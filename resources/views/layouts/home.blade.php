<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title> @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
</head>

<body>
    <div class="container">
        <header id="cabecalho">
            <div class="row">
            
                <div class="logo col-md-12 text-center">
                    <img src="img/logo.png" class="rounded mx-auto d-block" alt="logo">
                </div>
            </div>
            <div class="row">
                <div class="menu col-md-12 text-center mx-auto">
                    <nav id="menu">
                        <ul>
                            <li><a href="{{ route('home') }}">HOME</a></li>
                            <li><a href="{{ route('html') }}">HTML</a></li>
                            <li><a href="{{ route('javascript') }}">JAVASCRIPT</a></li>
                            <li><a href="{{ route('tema-css') }}">CSS</a></li>
                            <li><a href="{{ route('videos') }}">VIDEO AULAS</a></li>
                            <li><a href="{{ route('contato') }}">CONTATO</a></li>
                        </ul>
                    </nav>

                </div>
            </div>

        </header>
        <div class="main">
                @yield('conteudo')
            </div>
        

        <footer id="rodape">
            <div class="row">
                <div class="col-md-12 text-center mx-auto">
                <p><strong>TODOS OS DIREITOS SÃO RESERVADOS</strong></p>
            </div>
            </div>

        </footer>



    </div>










    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
