@extends('layouts.home')
@section('title','Home')
@section('conteudo')
<div class="row">
        <div class="item col-4 text-center">
            <img class="img-fluid" src="img/post-3.jpg" alt="html">
            <h3>Dicas de HTML</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias, hic! Expedita, repellendus
                modi! Cum, doloribus magni mollitia, necessitatibus nihil porro ullam ea iusto perferendis
                corporis, vero dolorum. Reiciendis, praesentium tempore.</p>
        </div>
        <div class="item-2 col-4 text-center">
            <img class="img-fluid" src="img/post-4.jpg" alt="mvc">
            <h3>O que é MVC </h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias, hic! Expedita, repellendus
                modi! Cum, doloribus magni mollitia, necessitatibus nihil porro ullam ea iusto perferendis
                corporis, vero dolorum. Reiciendis, praesentium tempore.</p>
        </div>
        <div class="item-3 col-4 text-center">
            <img src="img/post-5.jpg" alt="planejamento" class="img-fluid">
            <h3>Planejamento</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias, hic! Expedita, repellendus
                modi! Cum, doloribus magni mollitia, necessitatibus nihil porro ullam ea iusto perferendis
                corporis, vero dolorum. Reiciendis, praesentium tempore.</p>
        </div>
    </div>


    <section id="mensagem">
            <div class="row">
                <div class="col-md-10 text-center mx-auto">
                    <h1>FAÇA DIFERENTE...</h1>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vero quibusdam doloremque dolores? Ab quis
                        numquam nihil earum beatae eum culpa vitae? Expedita, reprehenderit. Pariatur, impedit cumque et
                        maiores officia Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aut placeat eaque quod
                        numquam non corrupti est voluptas alias quos laborum, adipisci qui reiciendis consectetur nesciunt
                        enim eligendi. Consequatur, a id.</p>
                    </div>
                </div>
    </section>










@endsection